# SQL database


## TABLE: Users

FIELD  | TYPE | COMMENTARY
------------- | ------------- | -------------
Id | INT  | Primary Key
FirstName | VARCHAR(255)  | -
LastName | VARCHAR(255)  | -
Email | VARCHAR(255)  | Value used for sign in
PhoneNumber | VARCHAR(255)  | -
Password | VARCHAR(255)  | -
Status | VARCHAR(255)  | Valued defining user type (customer, delivery man, professional)
Notify | BOOL  | -


## TABLE: BuyingCredentials

FIELD  | TYPE | COMMENTARY
------------- | ------------- | -------------
Id | INT  | Primary Key
Id_User | INT  | Foreign Key
CardName | VARCHAR(255)  | -
CardNumber | VARCHAR(255)  | -
CardCrypto | VARCHAR(255)  | -
CardValidity | VARCHAR(255)  | -
BuyingMethod | VARCHAR(255)  | -


## TABLE: UserAddress

FIELD  | TYPE | COMMENTARY
------------- | ------------- | -------------
Id | INT  | Primary Key
Id_User | INT  | Foreign Key
PostalCode | VARCHAR(255)  | -
StreetName | VARCHAR(255)  | -
City | VARCHAR(255)  | -
DefaultDelivery | BOOL  | -
DefaultFacturation | BOOL  | -


## TABLE: Staff

FIELD  | TYPE | COMMENTARY
------------- | ------------- | -------------
Id | INT  | Primary Key
Email | VARCHAR(255)  | Value used for sign in
Password | VARCHAR(255)  | -

