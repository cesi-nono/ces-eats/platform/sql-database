/*------------------------------------------------------------
-- Database: ceseats
------------------------------------------------------------*/
    CREATE DATABASE ceseats;
    GO
    USE ceseats;
    GO

/*------------------------------------------------------------
-- Table: User
-- Create a new table called '[User]' in schema '[ceseats]'
-- Drop the table if it already exists
------------------------------------------------------------*/
    -- Create the table in the specified schema
    CREATE TABLE Users
    (
        [Id] INT NOT NULL PRIMARY KEY, -- Primary Key column
        [FirstName] NVARCHAR(255) NOT NULL,
        [LastName] NVARCHAR(255) NOT NULL,
        [Email] NVARCHAR(255) NOT NULL,
        [PhoneNumber] NVARCHAR(255) NOT NULL,
        [Password] NVARCHAR(255) NOT NULL,
        [Status] NVARCHAR(255) NOT NULL,
        [Notify] INT NOT NULL
    );
    GO

/*------------------------------------------------------------
-- Table: BuyingCredentials
-- Create a new table called '[BuyingCredentials]' in schema '[ceseats]'
-- Drop the table if it already exists
------------------------------------------------------------*/
    -- Create the table in the specified schema
    CREATE TABLE BuyingCredentials
    (
        [Id] INT NOT NULL PRIMARY KEY, -- Primary Key column
        [Id_User] INT NOT NULL FOREIGN KEY REFERENCES Users(Id), -- Foreign Key column
        [CardName] NVARCHAR(255) NOT NULL,
        [CardNumber] NVARCHAR(255) NOT NULL,
        [CardCrypto] NVARCHAR(255) NOT NULL,
        [CardValidity] NVARCHAR(255) NOT NULL,
        [BuyingMethod] NVARCHAR(255) NOT NULL
    );
    GO


/*------------------------------------------------------------
-- Table: UserAddress
-- Create a new table called '[UserAddress]' in schema '[ceseats]'
-- Drop the table if it already exists
------------------------------------------------------------*/
    -- Create the table in the specified schema
    CREATE TABLE UserAddress
    (
        [Id] INT NOT NULL PRIMARY KEY, -- Primary Key column
        [Id_User] INT NOT NULL FOREIGN KEY REFERENCES Users(Id), -- Foreign Key column
        [PostalCode] NVARCHAR(255) NOT NULL,
        [StreetName] NVARCHAR(255) NOT NULL,
        [City] NVARCHAR(255) NOT NULL,
        [DefaultDelivery] INT NOT NULL,
        [DefaultFacturation] INT NOT NULL,
    );
    GO

/*------------------------------------------------------------
-- Table: Staff
-- Create a new table called '[Staff]' in schema '[ceseats]'
-- Drop the table if it already exists
------------------------------------------------------------*/
    -- Create the table in the specified schema
    CREATE TABLE Staff
    (
        [Id] INT NOT NULL PRIMARY KEY, -- Primary Key column
        [Email] NVARCHAR(255) NOT NULL,
        [Password] NVARCHAR(255) NOT NULL
    );
    GO

