for i in {1..50};
do
    /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P $SA_PASSWORD -d master -i $DATA/setup.sql
    if [ $? -eq 0 ]
    then
        echo "Setup.sql completed"
        break
    else
        echo "Not ready yet..."
        sleep 1
    fi
done

# DATABASE CREATED AND READY TO USE
