set -m

# Server starting in background
/opt/mssql/bin/sqlservr &

# Run import of schemas
echo "Running IMPORT SQL SCHEMAS"
$DATA/import-schemas.sh

# Run import of data
echo "Running IMPORT SQL DATA"
echo "Users:"
/opt/mssql-tools/bin/bcp $DB_NAME.dbo.Users in "$DATA/Users.csv" -c -t',' -S localhost -U sa -P $SA_PASSWORD
echo "BuyingCredentials:"
/opt/mssql-tools/bin/bcp $DB_NAME.dbo.BuyingCredentials in "$DATA/BuyingCredentials.csv" -c -t',' -S localhost -U sa -P $SA_PASSWORD
echo "UserAddress:"
/opt/mssql-tools/bin/bcp $DB_NAME.dbo.UserAddress in "$DATA/UserAddress.csv" -c -t',' -S localhost -U sa -P $SA_PASSWORD
echo "Staff:"
/opt/mssql-tools/bin/bcp $DB_NAME.dbo.Staff in "$DATA/Staff.csv" -c -t',' -S localhost -U sa -P $SA_PASSWORD

# Reattach server in foreground
fg %1